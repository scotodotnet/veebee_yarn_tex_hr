﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class MDFineRptDisplay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";

    string ShiftType1 = "";
    string FromDate;
    string ToDate;
    string unit = "";
    DateTime frmDate;
    DateTime toDate;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    string MonthStr = ""; string FinYrStr = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();


            MonthStr = Request.QueryString["Month"].ToString();
            FinYrStr = Request.QueryString["Year"].ToString();
            unit = Request.QueryString["Unit"].ToString();

            GetImproper();

        }
    }

    public void GetImproper()
    {
        string SSQL = "";
        DataTable AutoDT = new DataTable();

        AutoDT.Columns.Add("CompanyName");
        AutoDT.Columns.Add("LocationName");
        AutoDT.Columns.Add("MachineID");
        AutoDT.Columns.Add("ExistingCode");
        AutoDT.Columns.Add("FirstName");
        AutoDT.Columns.Add("DeptName");
        AutoDT.Columns.Add("Designation");
        AutoDT.Columns.Add("EmpCount");
        AutoDT.Columns.Add("ImpCount");
        AutoDT.Columns.Add("ImpAmt");

        DataTable DT = new DataTable();
        string Month = "";
        string CurntMnt = "";
        int Year;

        string MonthStr_IP = MonthStr;



        Month = "";
        Year = Convert.ToInt32(FinYrStr);//Convert.ToInt32(dt_Salary.Rows[i]["Financialyear"].ToString());
        if (MonthStr == "January")//dt_Salary.Rows[i]["Month"].ToString() == "January")
        {
        
            CurntMnt = "01";
        }
        if (MonthStr == "February")//dt_Salary.Rows[i]["Month"].ToString() == "February")
        {
        
            CurntMnt = "02";

        }
        if (MonthStr == "March")//dt_Salary.Rows[i]["Month"].ToString() == "March")
        {
            
            CurntMnt = "03";
        }
        if (MonthStr == "April")//dt_Salary.Rows[i]["Month"].ToString() == "April")
        {
            
            CurntMnt = "04";
        }
        if (MonthStr == "May")//dt_Salary.Rows[i]["Month"].ToString() == "May")
        {
           
            CurntMnt = "05";
        }
        if (MonthStr == "June")//dt_Salary.Rows[i]["Month"].ToString() == "June")
        {
           
            CurntMnt = "06";
        }
        if (MonthStr == "July")//dt_Salary.Rows[i]["Month"].ToString() == "July")
        {
            
            CurntMnt = "07";
        }
        if (MonthStr == "August")//dt_Salary.Rows[i]["Month"].ToString() == "August")
        {
          
            CurntMnt = "08";
        }
        if (MonthStr == "September")//dt_Salary.Rows[i]["Month"].ToString() == "September")
        {
       
            CurntMnt = "09";
        }
        if (MonthStr == "October")//dt_Salary.Rows[i]["Month"].ToString() == "October")
        {
           
            CurntMnt = "10";
        }
        if (MonthStr == "November")//dt_Salary.Rows[i]["Month"].ToString() == "November")
        {
          
            CurntMnt = "11";
        }
        if (MonthStr == "December")//dt_Salary.Rows[i]["Month"].ToString() == "December")
        {
            
            CurntMnt = "12";
            
        } string tdate = "";
        if ((MonthStr == "January") || (MonthStr == "March") || (MonthStr == "May") || (MonthStr == "July") || (MonthStr == "August") || (MonthStr == "October") || (MonthStr == "December"))
        {
            tdate = "31";
        }
        else if ((MonthStr == "April") || (MonthStr == "June") || (MonthStr == "September") || (MonthStr == "November"))
        {
            tdate = "30";
        }

        else if (MonthStr == "February")
        {
            int yrs = (Convert.ToInt32(FinYrStr) + 1);
            if ((yrs % 4) == 0)
            {
                tdate = "29";
            }
            else
            {
                tdate = "28";
            }
        }
      
        string fmdate = "01";


        FromDate  = fmdate + "/" + CurntMnt + "/" + FinYrStr.ToString();
        ToDate  = tdate + "/" + CurntMnt + "/" + FinYrStr.ToString();
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

       

        SSQL = "Select LT.MachineID,LT.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation,Count(LT.MachineID) as EmpCount,";
        SSQL = SSQL + "'' as ImpCount,'' as ImpAmt from Log_ImproperDays LT inner join Employee_Mst EM on LT.MachineID=EM.MachineID And LT.LocCode=EM.LocCode";
        SSQL = SSQL + " where LT.CompCode='" + SessionCcode + "' And LT.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,LT.Attn_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,LT.Attn_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " Group by LT.MachineID,LT.ExistingCode,EM.FirstName,EM.DeptName,EM.Designation";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            DataTable dt = new DataTable();

            SSQL = "Select * from Mst_LateFine Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            string ImpDays = "0";
            string ImpAmt="0";

            if (dt.Rows.Count != 0)
            {
                ImpDays = dt.Rows[0]["ImproperDays"].ToString();
                ImpAmt = dt.Rows[0]["ImproperAmt"].ToString();

            }

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                DataTable dt1 = new DataTable();
                SSQL = "Select * from Company_Mst ";
                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt1.Rows[0]["CompName"].ToString();

                if (Convert.ToInt32(DT.Rows[i]["EmpCount"].ToString()) > Convert.ToInt32(ImpDays))
                {
                    int ImpFinal = Convert.ToInt32(DT.Rows[i]["EmpCount"].ToString()) - Convert.ToInt32(ImpDays);

                    AutoDT.NewRow();
                    AutoDT.Rows.Add();

                    AutoDT.Rows[AutoDT.Rows.Count - 1]["CompanyName"] = name;
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["LocationName"] = SessionLcode;
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["MachineID"] = DT.Rows[i]["MachineID"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["ExistingCode"] = DT.Rows[i]["ExistingCode"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["FirstName"] = DT.Rows[i]["FirstName"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["DeptName"] = DT.Rows[i]["DeptName"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["Designation"] = DT.Rows[i]["Designation"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["EmpCount"] = DT.Rows[i]["EmpCount"].ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["ImpCount"] = ImpFinal.ToString();
                    AutoDT.Rows[AutoDT.Rows.Count - 1]["ImpAmt"] = ImpAmt.ToString();
                }
            }
        }

        if (AutoDT.Rows.Count != 0)
        {
            ds.Tables.Add(AutoDT);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/ImproperFine.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (FromDate != "" && ToDate != "")
            {
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate.ToString() + "'";
                report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate.ToString() + "'";
            }     

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
