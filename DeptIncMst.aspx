<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="DeptIncMst.aspx.cs" Inherits="DeptIncMst" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <ContentTemplate>
<!-- begin #content -->
    <div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Bio Metric</a></li>
				<li class="active">Incentive </li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Incentive Type </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Incentive Type</h4>
                        </div>
                        <div class="panel-body">
                         <!-- begin row -->
                          <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                        <!-- begin row -->
                          <div class="row">
                          
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								    <label>Department Type </label>
								    <asp:RadioButtonList ID="rdbType" runat="server" RepeatColumns="5" class="form-control">
                                          <asp:ListItem Selected="False" Text="All Department" style="padding-right:10px" Value="1"></asp:ListItem>
                                          <asp:ListItem Selected="true" Text="Spinning" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Incen. Min Days</label>
								  <asp:TextBox runat="server" ID="txtShiftInc_MinDays" class="form-control" Text="0"></asp:TextBox>
								  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               <div class="col-md-2">
								<div class="form-group">
								  <label>Food Allowance</label>
								  <asp:TextBox runat="server" ID="txtFoodAllowance" class="form-control" Text="0"></asp:TextBox>
								  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFoodAllowance" ValidChars="0123456789.">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                              
                              <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                          onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" onclick="btnClear_Click" 
                                         />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              </div>
                        <!-- end row -->
                     <!-- begin row -->
                         <div class="row">
                         <!-- begin col-4 -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Department</label><span class="mandatory">*</span>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" 
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" Display="Dynamic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                               
                            </div>
                         <!-- end col-4 -->
                         <!-- begin col-4 -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Designation</label><span class="mandatory">*</span>
                                    <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control select2" style="width:100%;" >
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ControlToValidate="ddlDesignation" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                         <!-- end col-4 -->
                         <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Incentive Amount</label>
								  <asp:TextBox runat="server" ID="txtIncAmt" class="form-control" Text="0"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtIncAmt" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789.">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                              <!-- end col-4 -->
                         <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>OT Incentive Amount</label>
								  <asp:TextBox runat="server" ID="txtOTIncAmt" class="form-control" Text="0"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtOTIncAmt" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                       TargetControlID="txtOTIncAmt" ValidChars="0123456789.">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                                <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnOTSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnOTSave_Click" />
									<asp:Button runat="server" id="btnOTClear" Text="Clear" class="btn btn-danger" onclick="btnOTClear_Click" 
                                         />
								 </div>
                               </div>
                              <!-- end col-4 -->
                         </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>DeptName</th>
                                                <th>Designation</th>
                                                <th>Inc Amt</th>
                                                <th>OT Inc Amt</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("DeptName")%></td>
                                        <td><%# Eval("Designation")%></td>
                                        <td><%# Eval("IncAmt")%></td>
                                        <td><%# Eval("OTIncAmt")%></td>
                                        <td>
                                                   <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%# Eval("Designation")%>' CommandName='<%# Eval("DeptCode")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("Designation")%>' CommandName='<%# Eval("DeptCode")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Incentive details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
    </ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

