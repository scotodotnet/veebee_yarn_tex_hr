﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class CommmisionVoucher_Report : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

   
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {



            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
         
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Commission Voucher Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

              Load_TransNo();
            }



        }
    }
    public void Load_TransNo()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddltransno.DataSource = dtempty;
        ddltransno.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select  CONVERT(varchar(10), TransID) as TransID  from CommisionVoucher_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddltransno.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["TransID"] = "-Select-";
        dr["TransID"] = "-Select-";
        dt.Rows.InsertAt(dr, 0);
        ddltransno.DataTextField = "TransID";
        ddltransno.DataValueField = "TransID";
        ddltransno.DataBind();

       
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("CommissionVoucherReportDisplay.aspx?TransID=" + ddltransno.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
    
    }
}
