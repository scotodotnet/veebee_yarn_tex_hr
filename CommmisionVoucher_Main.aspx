﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="CommmisionVoucher_Main.aspx.cs" Inherits="CommmisionVoucher_Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
<div id="content" class="content">
 <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Commission</a></li>
        <li><a href="javascript:;">Commission Voucher</a></li>
        <li class="active">Commission Voucher</li>
    </ol>
    <!-- end breadcrumb -->
     <!-- begin page-header -->
    <h1 class="page-header">Commission Voucher</h1>
    <!-- end page-header -->
    
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <div>
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Commission Voucher</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <asp:LinkButton ID="lbtnAdd" runat="server" class="btn btn-success" 
                                    onclick="lbtnAdd_Click">Add Details</asp:LinkButton>
                                
                            </div>
                        </div>
                          
                  
                        <div class="row">
                       
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>TransID</th>
                                                        <th>Trans Date</th>
                                                        <th>TokenNo</th>
                                                        <th>RefferalType</th>
                                                        <th>RefferalName</th>
                                                        <th>Amount</th>
                                                        <th>Edit</th>
                                                       
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("TransID")%></td>
                                                <td><%# Eval("TransDate")%></td>
                                                <td><%# Eval("TokenNo")%></td>
                                                 <td><%# Eval("ReferalType")%></td>
                                                <td><%# Eval("ReferalName")%></td>
                                                <td><%# Eval("Amount")%></td>
                                               
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("TransID")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("TransID")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Voucher?');">
                                                    </asp:LinkButton>
                                                    </td>
                                                    
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

