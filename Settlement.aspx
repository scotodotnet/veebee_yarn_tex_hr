﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Settlement.aspx.cs" Inherits="Settlement" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




    <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Settlement</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Settlement</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Settlement</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                        
                           <div class="col-md-4">
								<div class="form-group">
								 <label>Token No</label>
								<asp:DropDownList runat="server" ID="ddlTokeNo" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlTokeNo_SelectedIndexChanged" >
                                  </asp:DropDownList>
								</div>
                               </div>
                        
                        
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Employee No</label>
								 <asp:TextBox ID="txtEmployeeNo" class="form-control" runat="server"></asp:TextBox>
								</div>
                               </div>
                               
                               <div class="col-md-4">
								<div class="form-group">
								 <label>Existing No</label>
								 <asp:TextBox ID="txtExisting" class="form-control" runat="server"></asp:TextBox>
								</div>
                               </div>
                               
                              
                              </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Name</label>
														 
								 <asp:TextBox ID="txtEmployeeName"  runat="server"  class="form-control"  ></asp:TextBox>
								</div>
                               </div>
                                                              
                               <div class="col-md-4">
								<div class="form-group">
								 <label>Department</label>
								 <asp:DropDownList runat="server" ID="ddldept" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" >
                                 </asp:DropDownList>
								</div>
                               </div>
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Designation</label>
								 <asp:DropDownList runat="server" ID="ddlDesignation" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" >
                                 </asp:DropDownList>
								</div>
                               </div>
                        
                            
                        </div>
                       <!-- end row -->
                         <!-- begin row -->
                         
                              <div class="row">
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Date of Joining</label>
								 <asp:TextBox ID="txtDOJ" runat="server" class="form-control datepicker"></asp:TextBox>
								 </div>
                               </div>
                                 <div class="col-md-4">
								<div class="form-group">
								 <label>Address of the Employee</label>
								 <asp:TextBox ID="txtAddress" runat="server" class="form-control"></asp:TextBox>
								 </div>
                               </div>
                               
                               
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Date of leaving service</label>
								 <asp:TextBox ID="txtLeavingDate" runat="server" class="form-control datepicker"></asp:TextBox>
								 </div>
                               </div>
                               
                          
                               
                              </div>
                              
                        <div class="row">
                        
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Reason of leaving service</label>
								 <asp:DropDownList runat="server" ID="ddlResign" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" >
                                        <asp:ListItem Value="1" Text="True"> </asp:ListItem>
                                        <asp:ListItem Value="2" Text="Resigned"> </asp:ListItem>
                                 </asp:DropDownList>
								</div>
                               </div>
                        
                        <div class="col-md-4">
								<div class="form-group">
								 <label>Total period of service</label>
								 <asp:TextBox ID="txtTotalPeriod" class="form-control" runat="server"></asp:TextBox>
								</div>
                         </div>
                         
                         <div class="col-md-4">
								<div class="form-group">
								 <label>Total eligible years</label>
								 <asp:TextBox ID="txteligibleYear" class="form-control" AutoPostBack="true" runat="server" ></asp:TextBox>
                                        
								</div>
                         </div>
                         
                    
                         
                       
                        </div>
                         <div class="row" >
                         
                               <div class="col-md-4" >
								<div class="form-group" >
								 <%--<label >Last drawn wages</label>--%>
								 <asp:TextBox ID="txtwages" class="form-control" runat="server" visible="false"></asp:TextBox>
								</div>
                         </div>
                         
                            <div class="col-md-4">
								<div class="form-group">
						<%--		 <label>Amount of Gratuity payable</label>--%>
								 <asp:TextBox ID="txtGratuity" class="form-control" runat="server" visible="false"></asp:TextBox>
								</div>
                         </div>
                         
                         <div class="col-md-4">
								<div class="form-group">
								<%-- <label>Amount of Bonus payable</label>--%>
								 <asp:TextBox ID="txtBonus" class="form-control" runat="server" visible="false"></asp:TextBox>
								</div>
                         </div>
                         
                         
                         </div>
                         
                    
                         <div class="row" visible="false">
                         <div class="col-md-4">
								<div class="form-group">
								 <%--<label>Amount of leave wages payable</label>--%>
								 <asp:TextBox ID="txtAmountWage" class="form-control" runat="server" visible="false"></asp:TextBox>
								</div>
                         </div>
                         
                           <div class="col-md-4">
								<div class="form-group">
								<%-- <label>Total amount</label>--%>
								 <asp:TextBox ID="txtTotalAmt" class="form-control" runat="server" visible="false"></asp:TextBox>
								</div>
                         </div>
                         
                          <div class="col-md-4">
								<div class="form-group">
							<%--	 <label>Cheque amt of payable near by Rs 10/-</label>--%>
								 <asp:TextBox ID="txtRoundoff" class="form-control" runat="server" visible="false"></asp:TextBox>
								</div>
                         </div>
                         </div>
                        
                   
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" 
                                         class="btn btn-success" onclick="btnSave_Click" />
                                      
                                         <asp:Button runat="server" id="btnReport" Text="Reports" 
                                         class="btn btn-success" onclick="btnReport_Click"  />
                                         
                                         
                                           <asp:Button runat="server" id="btnBonus" Text="Bonus Calculation" 
                                         class="btn btn-success" onclick="btnBonus_Click"   />
							     </div>
                               </div>
                               
                               <div class="col-md-1">
								<div class="form-group">
								
								</div>
								</div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



</asp:Content>


