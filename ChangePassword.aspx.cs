﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Security.Cryptography;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ChangePassword : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    //TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionRights;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ERP Weaving Module :: Change Password";
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        //SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            
            
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string New_Password = UTF8Encryption(txtNewPassword.Text);
        string Conform_Password = UTF8Encryption(txtConformPassword.Text);
        bool Error = false;
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();

   
        if (!Error)
        {
            if (New_Password == Conform_Password)
            {

                Error = false;

            }
            else
            {
                Error = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Conform Password..');", true);
            }
        }

        query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + SessionUserID + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

      
        query = "Select * from MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode='" + SessionUserID + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
     


        if (!Error)
        {
            query = "update [" + SessionRights + "]..MstUsers set Password='" + New_Password + "' where UserCode='" + SessionUserID + "' And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            query = "update MstUsers set Password='" + New_Password + "' where UserCode='" + SessionUserID + "' And CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

              }

        if (!Error)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Password Changed Successfully..');", true);
        }

    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtNewPassword.Text = "";
        txtConformPassword.Text = "";
    }

    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
}
