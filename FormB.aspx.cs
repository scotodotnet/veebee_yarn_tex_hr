﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;



public partial class FormB : System.Web.UI.Page
{
    string SessionCcode = "";
    string SessionLcode = "";
    string SessionAdmin = "";
    string SessionCompanyName = "";
    string SessionLocationName = "";
    string SessionUserType = "";
    string SessionEpay = "";
    string yr = "";
    string SSQL = "";
    string CompanyName = "";
    DataTable DT_Sal = new DataTable();
    Boolean ErrFlag = false;
    BALDataAccess objdata = new BALDataAccess();
    ReportDocument rd = new ReportDocument();
    decimal PFAmount = 0;
    decimal ESIAmount = 0;
    decimal Gross = 0;
    Decimal Net = 0;
    DataSet ds = new DataSet();
    string Payslip_Folder = "Payslip";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        if (!IsPostBack)
        {
            Page.Title = "Spay Module | Report Form-B";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
            //li.Attributes.Add("class", "droplink active open");
        }
        //Wages = Request.QueryString["Wages"].ToString();
        //year = Request.QueryString["Year"].ToString();


        yr = Request.QueryString["yr"].ToString();

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        FORMB();



        if (DT_Sal.Rows.Count != 0)
        {
            ds.Tables.Add(DT_Sal);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("Payslip_IF/FORM_B.rpt"));

            DataTable DT_Company = new DataTable();
            SSQL = "";
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            DT_Company = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Company.Rows.Count != 0)
            {
                CompanyName = DT_Company.Rows[0]["CompName"].ToString();
            }



            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.DataDefinition.FormulaFields["CompName"].Text = "'" + CompanyName + "'";
            //report.DataDefinition.FormulaFields["Months"].Text = "'" + AttMonths + "'";
            report.DataDefinition.FormulaFields["Year"].Text = "'" + yr + "'";
            //report.DataDefinition.FormulaFields["Tdate"].Text = "'" + Tdate + "'";
            // report.DataDefinition.FormulaFields["Tdate"].Text = "'" + Tdate + "'";

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
        }
    }
            

    private void FORMB()
    {
        
        if (ErrFlag != true)
        {
            SSQL = "";
            SSQL = "select Count(Cast(EmpNo as int)) as TotalEmpNo,Month,Sum(Cast(GrossEarnings as Decimal )) as Gross,";
            SSQL = SSQL + "Sum(Cast(ProvidentFund as decimal)) as PF ,Sum(Cast(ESI as decimal)) as ESI";
            SSQL = SSQL + " from ["+ SessionEpay + "]..SalaryDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and  Year ='" + yr + "' group by Month,FromDate order by FromDate Asc";
            DT_Sal = objdata.RptEmployeeMultipleDetails(SSQL);
            

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Year Properly');", true);
        }

    }
}
