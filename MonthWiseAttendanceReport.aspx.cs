﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Web.UI;

public partial class MonthWiseAttendanceReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionEpay;
    string Division;
    string Date1_str;
    string Date2_str;
    string Status;


    string SSQL = "";

    DataTable mDataSet = new DataTable();

    DataTable AutoDataTable = new DataTable();
    int shiftCount;
    DateTime fromdate;
    DateTime todate;
    int dayCount;
    System.DateTime iDate;
    int daysAdded = 0;
    DataTable Datacells = new DataTable();
    Boolean isPresent;
    string SessionUserType;
    int grand;
    DataTable mLocalDS = new DataTable();

    string FromYear = "";
    string ToYear = "";
    string FromMonth = "";
    string ToMonth = "";

    string CatName = "";

    string FromDteMonth = "";

    string ToDteMonth = "";


    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Manual Attendance";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            //SessionUserType = Session["UserType"].ToString();
            Division = Request.QueryString["Division"].ToString();
            FromMonth = Request.QueryString["FromMonth"].ToString();
            ToMonth = Request.QueryString["ToMonth"].ToString();
            FromYear = Request.QueryString["FromYear"].ToString();
            ToYear = Request.QueryString["ToYear"].ToString();
            Status = Request.QueryString["Status"].ToString();
            //Fill_Manual_Day_Attd_Between_Dates();          

            Manual_Attendance_writeAttend();

        }
    }
    public void Fill_Manual_Day_Attd_Between_Dates()
    {
        try
        {
            string TableName = "";

            if (Status == "Pending")
            {
                TableName = "Employee_Mst_New_Emp";
            }

            else
            {
                TableName = "Employee_Mst";
            }
            fromdate = Convert.ToDateTime(Date1_str);
            todate = Convert.ToDateTime(Date2_str);
            int dayCount = (int)((todate - fromdate).TotalDays);
            if (dayCount > 0)
            {
                //if (category == "STAFF")
                //{
                SSQL = "";
                SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
                SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from " + TableName + " EM,ManAttn_Details MA where";
                SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
                SSQL = SSQL + " And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                SSQL = SSQL + " And MA.Compcode='" + SessionCcode + "' And MA.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date1_str + "', 105)";
                SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date2_str + "', 105)";
                //   SSQL = SSQL + " And EM.EmpCatCode = '" + SessionDivision + "'";
                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }
                if (SessionUserType == "2")
                {
                    SSQL = SSQL + " And EM.IsNonAdmin='1'";
                }
                SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";
                //}
                //else if (category == "LABOUR")
                //{
                //    SSQL = "";
                //    SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
                //    SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,ManAttn_Details MA where";
                //    SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
                //    SSQL = SSQL + " And EM.Compcode='" + CompanyCode.ToString() + "' And EM.LocCode='" + LocationCode.ToString() + "' And EM.IsActive='" + active + "'";
                //    SSQL = SSQL + " And MA.Compcode='" + CompanyCode.ToString() + "' And MA.LocCode='" + LocationCode.ToString() + "'";
                //    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date + "', 105)";
                //    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date2 + "', 105)";
                //    SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";
                //}


                DataTable dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dsEmployee.Rows.Count > 0)
                {
                    AutoDataTable.Columns.Add("EmpNo");
                    AutoDataTable.Columns.Add("DeptName");
                    AutoDataTable.Columns.Add("MachineID");
                    AutoDataTable.Columns.Add("ExistingCode");
                    AutoDataTable.Columns.Add("FirstName");

                    for (int i = 0; i < dsEmployee.Rows.Count; i++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                        AutoDataTable.Rows[i]["EmpNo"] = dsEmployee.Rows[i]["EmpNo"];
                        AutoDataTable.Rows[i]["DeptName"] = dsEmployee.Rows[i]["DeptName"];
                        AutoDataTable.Rows[i]["MachineID"] = dsEmployee.Rows[i]["MachineID"];
                        AutoDataTable.Rows[i]["ExistingCode"] = dsEmployee.Rows[i]["ExistingCode"];
                        AutoDataTable.Rows[i]["FirstName"] = dsEmployee.Rows[i]["FirstName"];

                    }


                    SSQL = "";
                    SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
                    SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')";

                    mDataSet = null;
                    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mDataSet.Rows.Count > 0)
                    {

                        shiftCount = mDataSet.Rows.Count * 2;
                        long iColVal = 0;

                        fromdate = Convert.ToDateTime(Date1_str);
                        todate = Convert.ToDateTime(Date2_str);
                        dayCount = (int)((todate - fromdate).TotalDays);
                        iColVal = dayCount + 1;
                        iDate = Convert.ToDateTime(Date1_str.ToString());


                        Datacells.Columns.Add("EmpNo");
                        //Datacells.Columns.Add("MachineID");
                        Datacells.Columns.Add("ExistingCode");
                        Datacells.Columns.Add("DeptName");
                        Datacells.Columns.Add("FirstName");
                        do
                        {
                            DateTime dayy = Convert.ToDateTime(fromdate.AddDays(daysAdded).ToShortDateString());
                            AutoDataTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                            Datacells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                            dayCount -= 1;
                            daysAdded += 1;
                            iDate = iDate.AddDays(1);
                            while (iDate > Convert.ToDateTime(Date2_str))
                            {
                                return;
                            }
                        }

                        while (true);
                    }
                }
            }


        }

        catch (Exception ex)
        {

        }
    }


    public void Manual_Attendance_writeAttend()
    {
        # region MonthFromDate

        if (FromMonth == "January")
        {
            FromDteMonth = "01-01";
        }
        else if (FromMonth == "February")
        {
            FromDteMonth = "01-02";
        }
        else if (FromMonth == "March")
        {
            FromDteMonth = "01-03";
        }
        else if (FromMonth == "April")
        {
            FromDteMonth = "01-04";
        }
        else if (FromMonth == "May")
        {
            FromDteMonth = "01-05";
        }
        else if (FromMonth == "June")
        {
            FromDteMonth = "01-06";
        }
        else if (FromMonth == "July")
        {
            FromDteMonth = "01-07";
        }
        else if (FromMonth == "August")
        {
            FromDteMonth = "01-08";
        }
        else if (FromMonth == "Sptember")
        {
            FromDteMonth = "01-09";
        }
        else if (FromMonth == "October")
        {
            FromDteMonth = "01-10";
        }
        else if (FromMonth == "November")
        {
            FromDteMonth = "01-011";
        }
        else if (FromMonth == "December")
        {
            FromDteMonth = "01-12";
        }
        #endregion
        #region MonthToDate
        if (ToMonth == "January")
        {
            ToDteMonth = "01-01";
        }
        else if (ToMonth == "February")
        {
            ToDteMonth = "01-02";
        }
        else if (ToMonth == "March")
        {
            ToDteMonth = "01-03";
        }
        else if (ToMonth == "April")
        {
            ToDteMonth = "01-04";
        }
        else if (ToMonth == "May")
        {
            ToDteMonth = "01-05";
        }
        else if (ToMonth == "June")
        {
            ToDteMonth = "01-06";
        }
        else if (ToMonth == "July")
        {
            ToDteMonth = "01-07";
        }
        else if (ToMonth == "August")
        {
            ToDteMonth = "01-08";
        }
        else if (ToMonth == "Sptember")
        {
            ToDteMonth = "01-09";
        }
        else if (ToMonth == "October")
        {
            ToDteMonth = "01-10";
        }
        else if (ToMonth == "November")
        {
            ToDteMonth = "01-011";
        }
        else if (ToMonth == "December")
        {
            ToDteMonth = "01-12";
        }
        #endregion

        string SSQL = "";
        DataTable DT = new DataTable();
        string FromDateMonth = "";
        string ToDateMonth = "";
        //DateTime FYear = Convert.ToDateTime(FromYear);
        FromDateMonth = FromDteMonth+"-"+FromYear;
        ToDateMonth= ToDteMonth+"-"+ToYear;

        try
        {
            SSQL = "Select ROW_NUMBER() OVER ( ORDER By P.ExistingCode) as ROW,P.ExistingCode,P.FirstName,P.CatName,P.BaseSalary,";
            SSQL = SSQL + " P.BankName,(+''''+P.AccountNo)AccountNo,P.YR,isnull(P.January,0)JAN,isnull(P.February,0)FEB,ISNULL(P.March,0)MAR,";
            SSQL = SSQL + " ISNULL(P.April,0)APR,isnull(P.May,0)MAY,isnull(P.June,0)JUN,ISNULL(P.July,0)JUL,ISNULL(P.August,0)AUG,";
            SSQL = SSQL + " ISNULL(P.September,0)SEP,ISNULL(P.October,0)OCT,ISNULL(P.November,0)NOV,ISNULL(P.December,0)DEC From (";

            SSQL = SSQL + " Select  Emp.ExistingCode,EMP.FirstName,EMP.Wages as CatName,EMP.BaseSalary,isnull(Emp.AccountNo,'')AccountNo,year(ATT.FromDate) as YR,EMP.BankName,";
            SSQL = SSQL + " ATT.Months,sum(Days) NO_day from Employee_Mst EMP ";

            SSQL = SSQL + " Inner Join [" + SessionEpay + "]..AttenanceDetails ATT on ATT.EmpNo=EMP.EmpNo ";
            SSQL = SSQL + " Where EMP.IsActive='Yes' ";

            if (FromYear != "" && ToYear != "")
            {
                SSQL = SSQL + " And ATT.FromDate between '" + Convert.ToDateTime(FromDateMonth).ToString("yyyy-MM-dd") + "' and '" + Convert.ToDateTime(ToDateMonth).ToString("yyyy-MM-dd") + "' ";
            }



            SSQL = SSQL + " Group by Emp.ExistingCode,EMP.FirstName,EMP.Wages,EMP.BaseSalary,Emp.AccountNo,ATT.FromDate,EMP.BankName,ATT.Months) A";

            SSQL = SSQL + " pivot( sum(A.NO_day) for Months in ( ";
            SSQL = SSQL + " January,  February,  March,  April,  May,  June, July,  August,  September,  October,  November,  December ";
            SSQL = SSQL + " ) ) as P";


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

        }
        catch (Exception ex)
        {

        }

        grid.DataSource = DT;
        grid.DataBind();
        string attachment = "attachment;filename=AbstractReprotMonth&YearWise.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='20'>");
        Response.Write("<a style=\"font-weight:bold\">ABSTRACT REPORT FOR MONTH AND YEAR WISE-" + Session["Lcode"].ToString() + "</a>");
        Response.Write("</td>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='20'>");
        Response.Write("<a style=\"font-weight:bold\">MONTH AND YEAR FROM  (" + FromDateMonth + "  To  " + ToDateMonth + ") </a>");
        Response.Write("</td>");
        //Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\"> FROM -" + Date1_str + "</a>");
        //Response.Write("&nbsp;&nbsp;&nbsp;");
        //Response.Write("<a style=\"font-weight:bold\"> TO -" + Date2_str + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();





    }

}
