﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Security.Cryptography;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class MstShiftRoster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionUserType;

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEpay;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Load_Shift();       
        }

        Load_Data();
    }

    private void Load_Shift()
    {
        SSQL = "";
        SSQL = "Select * from Shift_Mst SM where SM.CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt_Shift = new DataTable();
        dt_Shift = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataSource = dt_Shift;
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstShiftRoster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;
        if (txtRosterName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Entre the Roster Name Must!!!')", true);
            ErrFlag = true;
            return ;
        }
        if (ddlShift.SelectedValue == "-Select-")
        {          
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Choose the Shift Name')", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "delete from MstShiftRoster where id='" + hiddenID + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstShiftRoster where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Roster_Name='" + txtRosterName.Text + "' and Shift_order='" + ddlShift.SelectedValue + "'";
                DataTable dt_Check = new DataTable();
                dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Check.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Details Already Present!!!')", true);
                    ErrFlag = true;
                    return;
                }

            }
            if (!ErrFlag)
            {
                SSQL = "";
                SSQL = "insert into MstShiftRoster (Ccode,Lcode,Roster_Name,Shift_Order) values('" + SessionCcode + "','" + SessionLcode + "','" + txtRosterName.Text + "','" + ddlShift.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Shift Roster Saved Successfully')", true);

                }
                if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Shift Roster Updated Successfully')", true);

                }
                btnClear_Click(sender,e);
            }       
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtRosterName.Text = "";
        ddlShift.ClearSelection();
        btnSave.Text = "Save";
        Load_Data();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstShiftRoster where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and id='" + e.CommandName + "'";
        DataTable dt_get = new DataTable();
        dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_get.Rows.Count > 0)
        {
            txtRosterName.Text = dt_get.Rows[0]["Roster_Name"].ToString();
            ddlShift.SelectedValue = dt_get.Rows[0]["Shift_Order"].ToString();
            btnSave.Text = "Update";
            hiddenID.Value = dt_get.Rows[0]["id"].ToString();
        }         
    }   

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from mstShiftRoster where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and id='" + e.CommandName + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
    }
}