﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master"  AutoEventWireup="true" CodeFile="MedicalDetailsReport.aspx.cs" Inherits="MedicalDetailsReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">





 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>


<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Employee Profile</a></li>
				<li class="active">Medical Details Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Medical Details Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Medical DetailsReport</h4>
                        </div>
                        <div class="panel-body">
                     
								 <div class="row">
								  
                               
								 
							<div class="form-group col-md-4">
                                  <div class="form-group">
												  <label>FromDate</label>
								  <asp:TextBox ID="txtFrmdate" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtFrmdate" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
												
												</div>
												</div>
												
												    <div class="form-group col-md-4">
												     <div class="form-group">
                                                     <label>ToDate</label>
								 <asp:TextBox ID="txtTodate" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTodate" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
												</div>
												</div>
												</div>
                               <div class="row">
                                <div class="txtcenter">
								 <div class="form-group">
									
									<asp:Button runat="server" id="btnDetails" Text="Details" class="btn btn-danger" 
                                          onclick="btnDetails_Click"  />
									<asp:Button runat="server" id="btnAbstract" Text="Abstract" class="btn btn-primary" 
                                          onclick="btnAbstract_Click"  />
									<asp:Button runat="server" id="btnConsolidate" Text="Consolidate" class="btn btn-info" 
                                          onclick="btnConsolidate_Click"  />
									 
								 </div>
                                </div>
                               
                              <!-- end col-4 -->
                         
                              
                        <!-- end row -->
                    
                    
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>


</asp:UpdatePanel>


</asp:Content>