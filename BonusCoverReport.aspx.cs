﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class BonusCoverReport : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();


    string Date;
    string Date2;
    string Payfromdate;
    string PayTodate;
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    Int32 time_Check_dbl = 0;
    string Total_Time_get = "";

    DataTable Payroll_DS = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    string Year;
    string State;
    string Division;
    string WagesType;

    DataRow dtRow;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Bonus Cover Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            string SessionCcode = Session["Ccode"].ToString();
            string SessionLcode = Session["Lcode"].ToString();
            string SessionUserType = Session["Isadmin"].ToString();
            Year = Request.QueryString["Year"].ToString();
            Payfromdate = Request.QueryString["PayFromDate"].ToString();
            PayTodate = Request.QueryString["payToDate"].ToString();
            //string[] Fyear = Year.Split('-');
            //string Year1 = Fyear[1];

            string Year1 = Year;

            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");



            DataTable New = new DataTable();
            string Time_IN_Str = "";
            string Time_Out_Str = "";
            int time_Check_dbl = 0;
            string Total_Time_get = "";
            DataTable Payroll_DS = new DataTable();


            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("Ex.Code");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("CoverDate");
            AutoDTable.Columns.Add("PayType");
            AutoDTable.Columns.Add("PayAmount");




            DataTable mLocalDS = new DataTable();
            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < 1; iTabRow++)
            {
                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 1;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }
                // 'Employee Master
                SSQL = "";
                SSQL = "Select EM.* from Employee_Mst EM ";
                SSQL = SSQL + " inner join [VB_EPay_Payroll]..Bonus_Details BD on Em.CompCode COLLATE DATABASE_DEFAULT=BD.Ccode COLLATE DATABASE_DEFAULT";
                SSQL = SSQL + " And EM.LocCode COLLATE DATABASE_DEFAULT=BD.Lcode COLLATE DATABASE_DEFAULT And EM.ExistingCode COLLATE DATABASE_DEFAULT=BD.ExisistingCode COLLATE DATABASE_DEFAULT";
                SSQL = SSQL + " where EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                SSQL = SSQL + " And EM.IsActive='Yes'";

                SSQL = SSQL + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + Year1 + "'";
                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }

                SSQL = SSQL + " Order by EM.ExistingCode Asc";

                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                if (mDataSet.Rows.Count > 0)
                {
                    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                    {
                        //'Check Payroll Salary Cover

                        DataTable Salary_DS = new DataTable();
                        DataTable dt_Bonus = new DataTable();
                        //'Get Payroll EmpNo
                        string Payroll_EmpNo = "";
                        string Payroll_EmployeeType = "";
                        string Payroll_SalaryType = "";


                        SSQL = "Select EM.EmpNo,EM.Salary_Through as Salarythrough,ET.EmpTypeCd as EmployeeType from Employee_Mst EM inner join MstEmployeeType ET";
                        SSQL = SSQL + " on EM.Wages=ET.EmpType";
                        SSQL = SSQL + " where EM.MachineID='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "'";
                        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                        // SSQL = SSQL + " And OP.Ccode='" + SessionCcode  + "' And OP.Lcode='" + SessionLcode  + "'";
                        //  SSQL = SSQL + " And EM.EmpNo=OP.EmpNo";
                        Salary_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (Salary_DS.Rows.Count != 0)
                        {
                            Payroll_EmpNo = "";
                            Payroll_EmployeeType = "";
                            Payroll_SalaryType = "";
                            Payroll_EmpNo = Salary_DS.Rows[0]["EmpNo"].ToString();
                            Payroll_EmployeeType = Salary_DS.Rows[0]["EmployeeType"].ToString();
                            Payroll_SalaryType = Salary_DS.Rows[0]["Salarythrough"].ToString();

                            
                            double Final_Display_Net_Pay = 0;
                            DataTable CalCulate_Amt_DS = new DataTable();

                            DateTime payfromdate2 = Convert.ToDateTime(Payfromdate.ToString());
                            DateTime paytodate2 = Convert.ToDateTime(PayTodate.ToString());

                            if (Salary_DS.Rows.Count != 0)
                            {
                                //'Get Total Aamount

                                SSQL = "Select * from [VB_EPay_Payroll]..Bonus_Details where Ccode='"+SessionCcode +"' And Lcode='"+SessionLcode +"' ";
                                SSQL = SSQL + " And MachineID='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "' and Bonus_Year='"+Year1 +"' ";
                                dt_Bonus = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt_Bonus.Rows.Count != 0)
                                {
                                    Final_Display_Net_Pay = Convert.ToDouble(dt_Bonus.Rows[0]["Final_Bonus_Amt"].ToString());

                                }
                                else
                                {
                                    Final_Display_Net_Pay = 0;
                                }
                                AutoDTable.NewRow();
                                AutoDTable.Rows.Add();

                                AutoDTable.Rows[mStartINRow][14] = Final_Display_Net_Pay;


                                //'Get Cover Date Punch 
                                SSQL = "Select Min(Bonusout) as [TimeIN] from LogTime_BONUS Where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And Bonusout >='" + payfromdate2.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01' ";
                                SSQL = SSQL + " And Bonusout <='" + paytodate2.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' ";
                                SSQL = SSQL + " And MachineID='" + mDataSet.Rows[iRow]["MachineID_Encrypt"] + "'";
                                SSQL = SSQL + " Order By Min(Bonusout)";
                                Salary_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (Salary_DS.Rows.Count != 0)
                                {
                                    if (Salary_DS.Rows[0]["TimeIN"].ToString() != "")
                                    {
                                        AutoDTable.Rows[mStartINRow][7] = Salary_DS.Rows[0]["TimeIN"].ToString();
                                        AutoDTable.Rows[mStartINRow][12] = Salary_DS.Rows[0]["TimeIN"].ToString();
                                        AutoDTable.Rows[mStartINRow][13] = "PAYIN";
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[mStartINRow][7] = "";
                                        AutoDTable.Rows[mStartINRow][12] = "";
                                        AutoDTable.Rows[mStartINRow][13] = "PAYOUT";
                                    }
                                }
                                else
                                {
                                    AutoDTable.Rows[mStartINRow][7] = "";
                                    AutoDTable.Rows[mStartINRow][12] = "";
                                    AutoDTable.Rows[mStartINRow][13] = "PAYOUT";
                                }

                                AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                                AutoDTable.Rows[mStartINRow][3] = "Bonus Cover";
                                AutoDTable.Rows[mStartINRow][9] = mDataSet.Rows[iRow]["MachineID"].ToString();
                                AutoDTable.Rows[mStartINRow][1] = mDataSet.Rows[iRow]["Designation"].ToString();
                                AutoDTable.Rows[mStartINRow][2] = mDataSet.Rows[iRow]["TypeName"].ToString();
                                AutoDTable.Rows[mStartINRow][4] = Convert.ToInt32(mDataSet.Rows[iRow]["EmpNo"].ToString());
                                AutoDTable.Rows[mStartINRow][5] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                                AutoDTable.Rows[mStartINRow][6] = mDataSet.Rows[iRow]["CatName"].ToString();
                                AutoDTable.Rows[mStartINRow][10] = mDataSet.Rows[iRow]["TypeName"].ToString();
                                AutoDTable.Rows[mStartINRow][11] = mDataSet.Rows[iRow]["SubCatName"].ToString();


                                mStartINRow += 1;

                            }

                        }
                        else
                        {
                            Payroll_EmpNo = "";
                            Payroll_EmployeeType = "";
                            Payroll_SalaryType = "";
                        }

                    }
                }


            }


            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);


            string name = dt.Rows[0]["CompName"].ToString();

            DataTable table_DT = new DataTable();


            table_DT.Columns.Add("CompanyName");
            table_DT.Columns.Add("LocationName");
            table_DT.Columns.Add("ShiftDate");

            table_DT.Columns.Add("SNo");
            table_DT.Columns.Add("Dept");
            table_DT.Columns.Add("Type");
            table_DT.Columns.Add("Shift");
            table_DT.Columns.Add("Category");
            table_DT.Columns.Add("SubCategory");
            table_DT.Columns.Add("EmpCode");
            table_DT.Columns.Add("ExCode");
            table_DT.Columns.Add("Name");
            table_DT.Columns.Add("TimeIN");
            table_DT.Columns.Add("MachineID");
            table_DT.Columns.Add("PrepBy");
            table_DT.Columns.Add("PrepDate");
            table_DT.Columns.Add("CoverDate");
            table_DT.Columns.Add("ReportDate");
            table_DT.Columns.Add("PayType");
            table_DT.Columns.Add("PayAmount");

            DateTime now = DateTime.Now;
            int sno = 1;
            for (int iRow1 = 0; iRow1 < AutoDTable.Rows.Count; iRow1++)
            {

                dtRow = table_DT.NewRow();
                dtRow["CompanyName"] = name;
                dtRow["LocationName"] = SessionLcode;
                dtRow["ShiftDate"] = now.ToString("dd/MM/yyyy");
                dtRow["SNo"] = sno;
                dtRow["Dept"] = AutoDTable.Rows[iRow1][1].ToString();
                dtRow["Type"] = AutoDTable.Rows[iRow1][2].ToString();
                dtRow["Shift"] = AutoDTable.Rows[iRow1][3].ToString();
                dtRow["Category"] = AutoDTable.Rows[iRow1][10].ToString();
                dtRow["SubCategory"] = AutoDTable.Rows[iRow1][11].ToString();
                dtRow["EmpCode"] = AutoDTable.Rows[iRow1][4].ToString();
                dtRow["ExCode"] = AutoDTable.Rows[iRow1][5].ToString();
                dtRow["Name"] = AutoDTable.Rows[iRow1][6].ToString();

                if (AutoDTable.Rows[iRow1][7].ToString() != "")
                {
                    dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());
                }
                else
                {
                    dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());

                }



                dtRow["MachineID"] = AutoDTable.Rows[iRow1][9].ToString();
                dtRow["PrepBy"] = "User";
                dtRow["PrepDate"] = Date;

                dtRow["ReportDate"] = "BONUS COVER REPORT FROM : " + Year1 ;

                if (AutoDTable.Rows[iRow1][12].ToString() != "")
                {
                    DateTime dtt = Convert.ToDateTime(AutoDTable.Rows[iRow1][12].ToString());
                    dtRow["CoverDate"] = dtt.AddDays(0).ToString("dd/MM/yyyy");
                }
                else
                {
                    dtRow["CoverDate"] = "";
                }
                dtRow["PayType"] = AutoDTable.Rows[iRow1][13].ToString();
                dtRow["PayAmount"] = AutoDTable.Rows[iRow1][14].ToString();

                table_DT.Rows.Add(dtRow);
            }




            ds.Tables.Add(table_DT);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Salary_Cover_Report_Summary.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
