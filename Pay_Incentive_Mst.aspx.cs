﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Incentive_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            Load_MonthIncentive();
            Load_CivilIncentive();
            Load_Doffer_Amount();
        }
        Load_Data_Incentive();
        Load_Hostel_Data_Incentive();
    }

    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [VB_EPay_Payroll]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
       
    }

    private void Load_Doffer_Amount()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [VB_EPay_Payroll]..WorkerIncentive_mst_Doffer where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtDoffer_Allow_Amt.Text = DT.Rows[0]["DofferAmt"].ToString();
        }
    }
   
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from [VB_EPay_Payroll]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And MonthDays='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from [VB_EPay_Payroll]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MonthDays='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtDaysOfMonth.Text.Trim() == "") || (txtDaysOfMonth.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
                ErrFlag = true;
            }
            else if ((txtMinDaysWorked.Text.Trim() == "") || (txtMinDaysWorked.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Minimum Days Worked');", true);
                ErrFlag = true;
            }
            else if ((txtIncent_Amount.Text.Trim() == "") || (txtIncent_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Incentive Amount');", true);
                ErrFlag = true;
            }
            else if ((txtRegularElgDays.Text.Trim() == "") || (txtRegularElgDays.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Regular Eligible Days');", true);
                ErrFlag = true;
            }
            else if ((txtRegularMaxDays.Text.Trim() == "") || (txtRegularMaxDays.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Regular Max Days');", true);
                ErrFlag = true;
            }
            else if ((txtRegularMaxAmt.Text.Trim() == "") || (txtRegularMaxAmt.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Regular Max Amount');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select * from [VB_EPay_Payroll]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MonthDays='" + txtDaysOfMonth.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [VB_EPay_Payroll]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtDaysOfMonth.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [VB_EPay_Payroll]..WorkerIncentive_mst (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays,Minumum_Days,Max_Days,Max_Amt)";
                Query = Query + " values ('" + SessionCcode + "','" + SessionLcode + "','" + txtRegularElgDays.Text + "','" + txtIncent_Amount.Text + "',";
                Query = Query + " '" + txtDaysOfMonth.Text + "','" + txtMinDaysWorked.Text + "','" + txtRegularMaxDays.Text + "','" + txtRegularMaxAmt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

              
                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Data_Incentive();
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
                txtRegularElgDays.Text = "0";
                txtRegularMaxAmt.Text = "0";
                txtRegularMaxDays.Text = "0";
            }
        }
        catch (Exception Ex)
        { 
        
        }
    }

    protected void BtnHostelSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtHostelDaysMonth.Text.Trim() == "") || (txtHostelDaysMonth.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
                ErrFlag = true;
            }
            else if ((txtHostelElgbl_Days.Text.Trim() == "") || (txtHostelElgbl_Days.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Eligible Days');", true);
                ErrFlag = true;
            }
            else if ((txtHostelInct_Amt.Text.Trim() == "") || (txtHostelInct_Amt.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Incentive Amount');", true);
                ErrFlag = true;
            }
            else if ((txtHostelMaxDays.Text.Trim() == "") || (txtHostelMaxDays.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Max Days');", true);
                ErrFlag = true;
            }
            else if ((txtHostelMaxAmt.Text.Trim() == "") || (txtHostelMaxAmt.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Max Amount');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select * from [VB_EPay_Payroll]..WorkerIncentive_mst_Hostel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MonthDays='" + txtHostelDaysMonth.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [VB_EPay_Payroll]..WorkerIncentive_mst_Hostel where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtHostelDaysMonth.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [VB_EPay_Payroll]..WorkerIncentive_mst_Hostel (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays,Max_Days,Max_Amt)";
                Query = Query + " values ('" + SessionCcode + "','" + SessionLcode + "','" + txtHostelElgbl_Days.Text + "','" + txtHostelInct_Amt.Text + "',";
                Query = Query + " '" + txtHostelDaysMonth.Text + "','" + txtHostelMaxDays.Text + "','" + txtHostelMaxAmt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);


                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Hostel_Data_Incentive();
                txtHostelDaysMonth.Text = "0";
                txtHostelElgbl_Days.Text = "0";
                txtHostelInct_Amt.Text = "0";
                txtHostelMaxAmt.Text = "0";
                txtHostelMaxDays.Text = "0";
            }
        }
        catch (Exception Ex)
        {

        }
    }

    private void Load_Hostel_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [VB_EPay_Payroll]..WorkerIncentive_mst_Hostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1_Hostel.DataSource = DT;
        Repeater1_Hostel.DataBind();

    }

    protected void GridDeletehostelEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from [VB_EPay_Payroll]..WorkerIncentive_mst_Hostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And MonthDays='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from [VB_EPay_Payroll]..WorkerIncentive_mst_Hostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MonthDays='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Hostel_Data_Incentive();
    }

    protected void BtnDofferAllow_Amt_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtDoffer_Allow_Amt.Text.Trim() == "") || (txtDoffer_Allow_Amt.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
                ErrFlag = true;
            }
            
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select * from [VB_EPay_Payroll]..WorkerIncentive_mst_Doffer where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [VB_EPay_Payroll]..WorkerIncentive_mst_Doffer where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [VB_EPay_Payroll]..WorkerIncentive_mst_Doffer (Ccode,Lcode,DofferAmt)";
                Query = Query + " values ('" + SessionCcode + "','" + SessionLcode + "','" + txtDoffer_Allow_Amt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);


                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
            }
        }
        catch (Exception Ex)
        {

        }
    }
        
    protected void btnIncen_Full_Amt_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtIncent_Month_Full_Amount.Text.Trim() == "") || (txtIncent_Month_Full_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....!');", true);
                ErrFlag = true;
            }
         
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select Amt from [VB_EPay_Payroll]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [VB_EPay_Payroll]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }

                Query = "Insert Into [VB_EPay_Payroll]..HostelIncentive (Ccode,Lcode,Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtIncent_Month_Full_Amount.Text + "','" + WorkDays.Checked.ToString() + "','" + Hallowed.Checked.ToString() + "',";
                Query = Query + "'" + OTdays.Checked.ToString() + "','" + NFHdays.Checked.ToString() + "','" + NFHwork.Checked.ToString() + "','" + CalWorkdays.Checked.ToString() + "',";
                Query = Query + "'" + CalHallowed.Checked.ToString() + "','" + CalOTdays.Checked.ToString() + "','" + CalNFHdays.Checked.ToString() + "','" + CalNFHwork.Checked.ToString() + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
            }
        }
        catch (Exception Ex)
        {

        }
    }
    
    public void Load_MonthIncentive()
    {
        DataTable dt = new DataTable();
        Query = "Select Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays from [VB_EPay_Payroll]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtIncent_Month_Full_Amount.Text = dt.Rows[0]["Amt"].ToString();
            if (dt.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper()) { WorkDays.Checked = true; } else { WorkDays.Checked = false; }
            if (dt.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper()) { Hallowed.Checked = true; } else { Hallowed.Checked = false; }
            if (dt.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper()) { OTdays.Checked = true; } else { OTdays.Checked = false; }
            if (dt.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper()) { NFHdays.Checked = true; } else { NFHdays.Checked = false; }
            if (dt.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { NFHwork.Checked = true; } else { NFHwork.Checked = false; }

            if (dt.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper()) { CalWorkdays.Checked = true; } else { CalWorkdays.Checked = false; }
            if (dt.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper()) { CalHallowed.Checked = true; } else { CalHallowed.Checked = false; }
            if (dt.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper()) { CalOTdays.Checked = true; } else { CalOTdays.Checked = false; }
            if (dt.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHdays.Checked = true; } else { CalNFHdays.Checked = false; }
            if (dt.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHwork.Checked = true; } else { CalNFHwork.Checked = false; }
        }
        else
        {
            txtIncent_Month_Full_Amount.Text = "0.00";
            WorkDays.Checked = false; Hallowed.Checked = false; OTdays.Checked = false; NFHdays.Checked = false;
            NFHwork.Checked = false; CalWorkdays.Checked = false; CalHallowed.Checked = false; CalOTdays.Checked = false;
            CalNFHdays.Checked = false; CalNFHwork.Checked = false;
        }

    }
    protected void btnCivil_Incent_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = true;
            DataTable dt = new DataTable();
            if (txtEligible_Days.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
                ErrFlag = true;
            }
            if (txtCivil_Incen_Amt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Query = "";
                Query = Query + "select * from [VB_EPay_Payroll]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = Query + "delete from [VB_EPay_Payroll]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    SaveFlag = false;
                }
                Query = "Insert into [VB_EPay_Payroll]..CivilIncentive(Ccode,Lcode,Amt,ElbDays) values(";
                Query = Query + "'" + SessionCcode + "','" + SessionLcode + "','" + txtEligible_Days.Text + "','" + txtCivil_Incen_Amt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);

                }
                else 
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
                }
                txtEligible_Days.Text = "0";
                txtCivil_Incen_Amt.Text = "0";
                Load_CivilIncentive();
            }
        }
        catch (Exception ex)
        {
        }
    }
    
    public void Load_CivilIncentive()
    {
        DataTable dt = new DataTable();
        Query = "";
        Query = Query + "select * from [VB_EPay_Payroll]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtEligible_Days.Text = dt.Rows[0]["ElbDays"].ToString();
            txtCivil_Incen_Amt.Text = dt.Rows[0]["Amt"].ToString();
        }
    }
}
