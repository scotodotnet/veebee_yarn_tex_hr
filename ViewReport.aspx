﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewReport.aspx.cs" Inherits="ViewReport" %>

<%@ Register assembly="CrystalDecisions.Web, Version=12.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </form>
    <table>
         <asp:Panel runat="server" ID="Panel2" Visible="false">
                         <tr id="Tr2" runat="server" visible="false"> 
                         <td colspan="4">
                           <asp:GridView ID="BankGV" runat="server" AutoGenerateColumns="false">
                           <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                           <%-- <asp:TemplateField>
                                                                <HeaderTemplate>DESIGNATION</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ACCOUNT NO</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NET CREDIT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("RoundOffNetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ATM</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("ATM")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>BANK CHARGES</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCharges" runat="server" Text="0.0"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            
                                                           <%-- <asp:TemplateField>
                                                                <HeaderTemplate>TOTAL</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text="0.0"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        </Columns>
                           </asp:GridView>
                         </td>
                         </tr>
                       </asp:Panel>
    </table>
</body>
</html>
