﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
public partial class ImproperWorkersBWDates : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();
            TokenNo = Request.QueryString["TokenNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            string SessionUserType_1 = SessionUserType;

            Between_Date_Query_Output();
        } 
    }
    private void Between_Date_Query_Output()
    {
        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

      
        AutoDTable.Columns.Add("EmployeeNo");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("DeptName");
      
        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        int intI = 0;
        int intK = 0;
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.Day.ToString()));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

      //  AutoDTable.Columns.Add("Total_Days");

        string query = "";

        query = "Select * from " + TableName + "  where LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "' And IsActive='Yes'  ";
        if (TokenNo != "")
        {
            query = query + "and ExistingCode='" + TokenNo + "'";
        }
        if (WagesType != "-Select-")
        {
            query = query + " And Wages='" + WagesType + "'";
            
        }
        if (Division != "-Select-")
        {
            query = query + " And EM.Division = '" + Division + "'";
        }
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        //query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff," + Query_header + " from ( ";
        //query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,DATENAME(dd, Attn_Date) as Day_V, ";
        //query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 THEN 'WH/X' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
        //query = query + " else (CASE WHEN LD.Present = 1.0 THEN 'X' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
        //query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        //query = query + " where LD.LocCode='" + SessionLcode + "' ";
        //query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
        //query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
        //if (TokenNo != "")
        //{
        //    query = query + " And LD.ExistingCode='" + TokenNo + "' And EM.ExistingCode='" + TokenNo + "'";
        //}
        //if (WagesType != "-Select-")
        //{
        //    query = query + " And EM.Wages='" + WagesType + "'";
        //    query = query + " And LD.Wages='" + WagesType + "'";
        //}
        //if (Division != "-Select-")
        //{
        //    query = query + " And EM.Division = '" + Division + "'";
        //}
        //query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        //query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        //query = query + " order by ExistingCode Asc";





                    for (int j1 = 0; j1 < dt.Rows.Count; j1++)
                {

                    if (dt.Rows[j1]["MachineID"].ToString() == "92")
                    {
                        string MachineID = "92";
                    }
                    if (dt.Rows[j1]["MachineID"].ToString() == "9578")
                    {
                        string MachineID = "9578";
                    }

                    intI = 4;
                    double Worked_Days_Count = 0;
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[j1]["MachineID"].ToString();
                    AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[j1]["ExistingCode"].ToString();
                    AutoDTable.Rows[intK]["Name"] = dt.Rows[j1]["FirstName"].ToString();
                    AutoDTable.Rows[intK]["DeptName"] = dt.Rows[j1]["DeptName"].ToString();


                    for (int intCol = 0; intCol <= daysAdded - 1; intCol++)
                    {
                        DateTime dtime = date1.AddDays(intCol);
                        DateTime dtime1 = dtime.AddDays(1);

                        Date_Value_Str = dtime.ToString("yyyy/MM/dd");
                        Date_value_str1 = dtime1.ToString("yyyy/MM/dd");

                        DataTable dt2 = new DataTable();
                        SSQL = "select MachineID,ExistingCode,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                        SSQL = SSQL + "Total_Hrs,Present_Absent from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                         SSQL = SSQL + " and MachineID='"+dt.Rows[j1]["MachineID"].ToString()+"' ";
                        if (Division != "-Select-")
                        {
                            SSQL = SSQL + " And Division = '" + Division + "'";
                        }
                        //  SSQL = SSQL + " and TimeIN!='' and TimeOUT!='' ";

                        //SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
                        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);


                        if (dt2.Rows.Count != 0)
                        {

                            if (dt2.Rows[0]["TimeIN"].ToString() != "")
                            {
                                SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + dt.Rows[j1]["MachineID_Encrypt"].ToString() + "' ";
                                SSQL = SSQL + " and TimeIN >='" + Date_Value_Str + " " + "04:00" + "'";
                                SSQL = SSQL + " and TimeIN <='" + Date_value_str1 + " " + "04:00" + "'";
                                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (dt1.Rows.Count <= 0)
                                {
                                    string Color = "";
                                    if (dt2.Rows[0]["Present_Absent"].ToString() == "Present")
                                    {
                                        Color = "green";
                                    }
                                    else if (dt2.Rows[0]["Present_Absent"].ToString() == "Absent")
                                    {
                                        Color = "red";
                                    }
                                    else if (dt2.Rows[0]["Present_Absent"].ToString() == "Half Day")
                                    {
                                        Color = "green";
                                    }
                                    AutoDTable.Rows[intK][intI] = "<span style=color:" + Color + "><b>" + "A/" + dt2.Rows[0]["Present_Absent"].ToString() + "</b></span>";
                                   
                                }
                            }

                        }
                        
                        SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + dt.Rows[j1]["MachineID_Encrypt"].ToString() + "' ";
                        SSQL = SSQL + " and TimeIN >='" + Date_Value_Str + " " + "04:00" + "'";
                        SSQL = SSQL + " and TimeIN <='" + Date_value_str1 + " " + "04:00" + "'";
                        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dt1.Rows.Count != 0)
                        {
                            if (dt1.Rows[0]["TimeIN"].ToString() != "")
                            {
                                SSQL = "select MachineID,ExistingCode,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                                SSQL = SSQL + "Total_Hrs,Present_Absent from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " and MachineID='" + dt.Rows[j1]["MachineID"].ToString() + "' ";
                                if (Division != "-Select-")
                                {
                                    SSQL = SSQL + " And Division = '" + Division + "'";
                                }
                                //  SSQL = SSQL + " and TimeIN!='' and TimeOUT!='' ";

                                //SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
                                dt2 = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (dt2.Rows.Count != 0)
                                {
                                    if (dt2.Rows[0]["TimeIN"].ToString() == "")
                                    {
                                        string Color = "";
                                        if (dt2.Rows[0]["Present_Absent"].ToString() == "Present")
                                        {
                                            Color = "green";
                                        }
                                        else if(dt2.Rows[0]["Present_Absent"].ToString() == "Absent")
                                        {
                                            Color = "red";
                                        }
                                        else if (dt2.Rows[0]["Present_Absent"].ToString() == "Half Day")
                                        {
                                            Color = "green";
                                        }
                                        
                                        AutoDTable.Rows[intK][intI] = "<span style=color:" +Color+ "><b>" + "A/" + dt2.Rows[0]["Present_Absent"].ToString() + "</b></span>";
                                    }
                                }
                                else if (dt2.Rows.Count <= 0)
                                {
                                    AutoDTable.Rows[intK][intI] = "<span style=color:red><b>" + "A/" +"Absent"+ "</b></span>";
                                   
                                }
                            }


                        }
                        
                        SSQL = "select MachineID,ExistingCode,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
                        SSQL = SSQL + "Total_Hrs,Present_Absent from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " and MachineID='" + dt.Rows[j1]["MachineID"].ToString() + "' ";
                        if (Division != "-Select-")
                        {
                            SSQL = SSQL + " And Division = '" + Division + "'";
                        }
                        //  SSQL = SSQL + " and TimeIN!='' and TimeOUT!='' ";

                        //SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
                        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);

                        SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + dt.Rows[j1]["MachineID_Encrypt"].ToString() + "' ";
                        SSQL = SSQL + " and TimeIN >='" + Date_Value_Str + " " + "04:00" + "'";
                        SSQL = SSQL + " and TimeIN <='" + Date_value_str1 + " " + "04:00" + "'";
                        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dt2.Rows.Count != 0)
                        {

                            if (dt2.Rows[0]["TimeIN"].ToString() != "")
                            {
                                if (dt1.Rows.Count != 0)
                                {
                                    if (dt1.Rows[0]["TimeIN"].ToString() != "")
                                    {
                                      
                                        AutoDTable.Rows[intK][intI] = "<span style=color:green><b>" + "P/" + dt2.Rows[0]["Present_Absent"].ToString() + "</b></span>";
                                       
                                    }
                                }
                            }
                        }

                        if (dt2.Rows.Count != 0)
                        {
                            if (dt2.Rows[0]["TimeIN"].ToString() == "")
                            {
                                if (dt1.Rows.Count <= 0)
                                {
                                    AutoDTable.Rows[intK][intI] = "<span style=color:red><b>" + "A/" + "Absent" + "</b></span>";

                                }

                            }
                        }
                        else if (dt2.Rows.Count <= 0)
                        {
                            if (dt1.Rows.Count <= 0)
                            {
                                AutoDTable.Rows[intK][intI] = "<span style=color:red><b>" + "A/" + "Absent" + "</b></span>";

                            }
                        }

                     intI += 1;


                }
               
               

                intK += 1;
                   
                   
                }
            
        if (AutoDTable.Rows.Count != 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=MISMATCHED WORKHOUSEIN BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">MISMATCHED WORKHOUSEIN BETWEEN DATES -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
        }

    }

}
