﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class EducationHisRpt : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Training Details Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

                Load_Data_EducationList();
            }
        }
    }

    private void Load_Data_EducationList()
    {
        string query = "";
        DataTable DT = new DataTable();

        ddlPursuing.Items.Clear();
        query = "Select Education from MstEducation where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlPursuing.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["Education"] = "-Select-";
        dr["Education"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlPursuing.DataTextField = "Education";
        ddlPursuing.DataValueField = "Education";
        ddlPursuing.DataBind();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string Course = "";
        string Status = "";

        if (ddlPursuing.SelectedItem.Text != "-Select-")
        {
            Course = ddlPursuing.SelectedItem.Text;
        }

        if (ddlStatus.SelectedItem.Text != "-Select-")
        {
            Status = ddlStatus.SelectedItem.Text;
        }

        ResponseHelper.Redirect("EducationHistRptDisplay.aspx?Course=" + Course + "&Status=" + Status, "_blank", "");
    }
}
