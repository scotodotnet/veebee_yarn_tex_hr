﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DocumentClass
/// </summary>
public class DocumentClass
{
	public DocumentClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _EmployeeNo;

    public string EmployeeNo
    {
        get { return _EmployeeNo; }
        set { _EmployeeNo = value; }
    }

    private string _EmployeeName;

    public string EmployeeName
    {
        get { return _EmployeeName; }
        set { _EmployeeName = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }

    private string _DocumentType;

    public string DocumentType
    {
        get { return _DocumentType; }
        set { _DocumentType = value; }
    }

    private string _DocumentNo;

    public string DocumentNo
    {
        get { return _DocumentNo; }
        set { _DocumentNo = value; }
    }

    private string _DocumentDesc;

    public string DocumentDesc
    {
        get { return _DocumentDesc; }
        set { _DocumentDesc = value; }
    }

    private string _Pathfile;

    public string Pathfile
    {
        get { return _Pathfile; }
        set { _Pathfile = value; }
    }

    private string _FileName;

    public string FileName
    {
        get { return _FileName; }
        set { _FileName = value; }
    }
}
