﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MedicalDetails.aspx.cs" Inherits="MedicalDetails" Title="Medical Record" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();

     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<%--Supplier Code 3 Select List Script Start--%>    
    <script type="text/javascript">
         $(document).ready(function () {
            initializer_Supp_Code3();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Supp_Code3);
        });
        function initializer_Supp_Code3() {
            $("#<%=txtDetails.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: "List_Service.asmx/GetMedicalCode_Det",
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],                                     
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                  $("#<%=txtDetails.ClientID %>").val(i.item.val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtDetails.ClientID %>").val(i.item.label);;  
                      
                   return false;
                  }
              });
          }
      </script>
<%--Supplier Code 3 Select List Script End--%>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Employee Profile</a></li>
				<li class="active">Medical Record</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Medical Record</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Medical Record</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Trans ID</label>
								  <asp:Label runat="server" ID="txtTransID" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Date</label>
								  <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker" ></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtDate" ValidChars="0123456789/">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Medical GPOUT No</label>
								  <asp:DropDownList runat="server" ID="ddlGPOUTNo" class="form-control select2" 
                                        AutoPostBack="true" onselectedindexchanged="ddlGPOUTNo_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:HiddenField runat="server" ID="txtGPOUTDate" />
								 <asp:RequiredFieldValidator ControlToValidate="ddlGPOUTNo" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                               </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-2 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Token No</label>
								   <asp:DropDownList runat="server" ID="ddlTokenNo" class="form-control select2" 
                                        AutoPostBack="true" onselectedindexchanged="ddlTokenNo_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlTokenNo" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-2 -->
                              <!-- begin col-2 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Machine ID</label>
								  <asp:Label runat="server" ID="txtMachineID" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                              <!-- end col-2 -->
                               <div class="col-md-4">
								<div class="form-group">
								  <label>Reason</label>
								  <asp:TextBox runat="server" ID="txtReason" class="form-control"></asp:TextBox>
								</div>
                               </div>
                                <!-- begin col-2 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Doctor Name</label>
								   <asp:DropDownList runat="server" ID="ddlDoctorName" class="form-control select2">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlDoctorName" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-2 -->
                                </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                          <div class="col-md-4">
								<div class="form-group">
								  <label>Medical Details</label>
								  <asp:TextBox runat="server" ID="txtDetails" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtDetails" Display="Dynamic"  ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                                <!-- end col-4 -->
                                 <div class="col-md-2">
								<div class="form-group">
								  <label>Amount</label>
								  <asp:TextBox runat="server" ID="txtAmount" class="form-control"></asp:TextBox>
								   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtAmount" ValidChars="0123456789.">
                                  </cc1:FilteredTextBoxExtender>
                                  <asp:RequiredFieldValidator ControlToValidate="txtAmount" Display="Dynamic"  ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                               <div class="col-md-4">
								<div class="form-group">
								  <label>Remarks</label>
								  <asp:TextBox runat="server" ID="txtRemarks" class="form-control"></asp:TextBox>
								   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers,LowercaseLetters,UppercaseLetters"
                                              TargetControlID="txtRemarks" ValidChars=" ">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                                <!-- end col-4 -->
                               
                          </div>
                         <!-- end row -->
                         <div class="row">
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							    <div class="form-group">
								    <br />
                                    <asp:FileUpload ID="filUpload" runat="server"  CssClass="btn btn-default fileinput-button" style="margin-bottom: 20px;"  />
                                </div>
                            </div>
                            <div class="col-md-2">
                                     <div class="form-group">
                                        <asp:Button ID="btnAdd" runat="server" class="btn btn-success" style="margin-top: 16%;" Text="ADD" ValidationGroup="Validate_Field1" onclick="btnAdd_Click"/>
                                     </div>
                                 </div>
                            <!-- end col-4 -->
                        </div>
                       <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Token No</th>
                                                <th>MachineID</th>
                                                <th>Reason</th>
                                                <th>Dr. Name</th>
                                                <th>Amount</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("TokenID")%></td>
                                        <td><%# Eval("MachineID")%></td>
                                        <td><%# Eval("Reason")%></td>
                                        <td><%# Eval("DoctorName")%></td>
                                        <td><%# Eval("Amount")%></td>
                                        <td>
                                           
                                            <asp:LinkButton ID="btnFile_Down" runat="server" Text="" OnCommand="GridFileOpenClick"
                                             class="btn btn-success btn-sm fa fa-file-photo-o" CommandArgument='<%# Eval("Path_Name")%>' 
                                             CommandName='<%# Eval("File_Name")%>'></asp:LinkButton>
                                            
                                            
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("TokenID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this TokenNo details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                       ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnAdd" />
</Triggers>
</asp:UpdatePanel>



</asp:Content>

