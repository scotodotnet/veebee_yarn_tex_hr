﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MDDashBoard.aspx.cs" Inherits="MDDashBoard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:UpdatePanel ID="UpdatePanel5" runat="server">
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			
			<!-- begin page-header -->
			<h1 class="page-header">MD DashBoard </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
		<div class="row">
		<!-- Current Year Employee Join Unit Wise Start -->
        <div class="col-sm-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h5> <label>Employee New Joinee / Left Employee</label></h5>
                </div>
                <div class="panel-body">
                    <div>
                        <div class="row">
                           <div class="col-md-4">
					           <asp:DropDownList ID="txtEmpJoinUnit" runat="server" class="" 
                                   AutoPostBack="true" onselectedindexchanged="txtEmpJoinUnit_SelectedIndexChanged">
                               </asp:DropDownList>
					         </div>
			            </div>   
                     <div class="row">
                       <div class="col-md-3">
					      <label>New Joinee : </label>
					   </div>
					    <div class="col-md-2">
					      <asp:Label ID="txtNewJoin" runat="server" Text="0" style="background-color:Yellow"></asp:Label>
					   </div>
					    <div class="col-md-3">
					      <label>Left Employee : </label>
					   </div>
					    <div class="col-md-2">
					      <asp:Label ID="txtLeft" runat="server" Text="0" style="background-color:Yellow"></asp:Label>
					   </div>
					   <div class="col-md-2"></div>
			        </div>  
			                             
                     <div>
                        
                            <asp:Chart ID="Current_Year_Emp_Join" runat="server" Compression="100" 
                                EnableViewState="True" Width="470px">
                                <%--<Legends>
                                <asp:Legend LegendStyle="Table"></asp:Legend>
                                </Legends>--%>
                               <%-- <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">style="color: #3cde50;"
                                    </asp:Series>
                                    <asp:Series Name="Series2" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                    
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>--%>
                            </asp:Chart>
                            
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 -->
        <!-- Current Year Employee Join Unit Wise End -->
        
        <!-- Employee Recruitment Wise Start -->
        <div class="col-sm-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h5> <label>Employee Recruitment </label></h5>  
                </div>
                <div class="panel-body">
                    <div> 
                    <div class="row">
                       <div class="col-md-4">
					       <asp:DropDownList ID="txtLocation" runat="server" class="" 
                               AutoPostBack="true" onselectedindexchanged="txtLocation_SelectedIndexChanged">
                           </asp:DropDownList>
					     </div>
			        </div>  
			                     
                        <div>
                            <asp:Chart ID="Current_Year_Emp_Left" runat="server" Compression="100" 
                                EnableViewState="True" Width="470px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
        <!-- col-sm-6 -->
        <!-- Employee Recruitment Wise End -->
        </div><!-- Row -->
        <!-- begin row -->
		<div class="row">
		<!-- Employee Trainee Module Start -->
		 <div class="col-sm-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h5> <label>Employee Status </label></h5>
                </div>
                <div class="panel-body">
                    <div> 
                    <div class="row">
                       <div class="col-md-4">
					       <asp:DropDownList ID="txtlevelLoc" runat="server" class="" 
                               AutoPostBack="true" onselectedindexchanged="txtlevelLoc_SelectedIndexChanged">
                           </asp:DropDownList>
					     </div>
			        </div>                   
                        <div>
                            <asp:Chart ID="Employee_Trainee_Level" runat="server" Compression="100" 
                                EnableViewState="True" Width="470px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Funnel" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
        <!-- Employee Trainee Module End -->
        
        <!-- Salary Strength Unitwise Start -->
		 <div class="col-sm-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h5> <label>Salary Strength Unitwise </label></h5>
                </div>
                <div class="panel-body">
                    <div> 
                   
                        <div>
                            <asp:Chart ID="Salary_Strength_Unitwise" runat="server" Compression="100" 
                                EnableViewState="True" Width="470px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
        <!-- Salary Strength Unitwise End -->
		</div>
       <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

