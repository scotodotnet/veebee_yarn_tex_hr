﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DepartmentAbstractRpt : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_Change();
            }
        }
    }

    public void NonAdminGetAttdDayWise_Change()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        //SSQL = SSQL + " And AM.CompCode='" + Session["SessionCcode"].ToString() + "' ANd AM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        //SSQL = SSQL + " And MG.CompCode='" + Session["SessionCcode"].ToString() + "' ANd MG.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        SSQL = SSQL + " And EM.Eligible_PF='1' ";
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        
        if (AutoDTable.Rows.Count != 0)
        {
            
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                if(AutoDTable.Rows[i]["SubCatName"].ToString().ToUpper() == "INSIDER" )
                {
                    SSQL = "Insert into Count (Department,Shift,SubCategory) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "')";
                }
                else
                {
                    SSQL = "Insert into Count (Department,Shift,OutCat) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "')";
                }
            }

            //New Query
            SSQL = "SELECT Dept,isnull(GENERAL,0) as GEN,isnull(SHIFT1,0) as Sht1,isnull(SHIFT2,0) as Sht2,isnull(SHIFT3,0) as Sht3,";
            SSQL = SSQL + " (isnull(GENERAL,0) + isnull(SHIFT1,0) + isnull(SHIFT2,0) + isnull(SHIFT3,0)) as All_Sht_Total";
            SSQL = SSQL + " FROM (";
            SSQL = SSQL + " Select Department as Dept,Shift, (Count(SubCategory) + Count(OutCat)) as Total";
            SSQL = SSQL + " from Count where SubCategory='INSIDER' OR OutCat='OUTSIDER'";
            SSQL = SSQL + " group by Department,Shift";
            SSQL = SSQL + " ) as s";
            SSQL = SSQL + " PIVOT ( sum(Total) FOR [Shift] IN ([GENERAL], [SHIFT1], [SHIFT2], [SHIFT3]) ) AS pivot_tbl order by Dept";

            DataCell = objdata.RptEmployeeMultipleDetails(SSQL);

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Day_Department_Abstract.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //Get Company Name
            SSQL = "Select * from Company_Mst Where CompCode='" + SessionCcode + "'";
            DataTable DT_For = new DataTable();
            DT_For = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_For.Rows.Count != 0)
            {
                report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + DT_For.Rows[0]["CompName"].ToString() + "'";
            }
            report.DataDefinition.FormulaFields["Location_Name"].Text = "'" + SessionLcode.ToString() + "'";
            if (Division.ToUpper().ToString() != "-Select-".ToUpper().ToString())
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + Division.ToString() + "'";
            }
            report.DataDefinition.FormulaFields["Date"].Text = "'" + Date.ToString() + "'";

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
    public void GetAttdDayWise_Change()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
        //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        //SSQL = SSQL + " And AM.CompCode='" + Session["SessionCcode"].ToString() + "' ANd AM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        //SSQL = SSQL + " And MG.CompCode='" + Session["SessionCcode"].ToString() + "' ANd MG.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        if (AutoDTable.Rows.Count != 0)
        {
              SSQL = "Delete from Count";
              objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                if (AutoDTable.Rows[i]["SubCatName"].ToString().ToUpper() == "INSIDER")
                {
                    SSQL = "Insert into Count (Department,Shift,SubCategory) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "')";
                }
                else
                {
                    SSQL = "Insert into Count (Department,Shift,OutCat) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "')";
                }
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            //New Query
            SSQL = "SELECT Dept,isnull(GENERAL,0) as GEN,isnull(SHIFT1,0) as Sht1,isnull(SHIFT2,0) as Sht2,isnull(SHIFT3,0) as Sht3,isnull(SHIFT4,0) as Sht4,isnull(SHIFT5,0) as Sht5,isnull(SHIFT6,0) as Sht6,";
            SSQL = SSQL + " (isnull(GENERAL,0) + isnull(SHIFT1,0) + isnull(SHIFT2,0) + isnull(SHIFT3,0)+isnull(SHIFT4,0)+isnull(SHIFT5,0)+isnull(SHIFT6,0)) as All_Sht_Total";
            SSQL = SSQL + " FROM (";
            SSQL = SSQL + " Select Department as Dept,Shift, (Count(SubCategory) + Count(OutCat)) as Total";
            SSQL = SSQL + " from Count where SubCategory='INSIDER' OR OutCat='OUTSIDER'";
            SSQL = SSQL + " group by Department,Shift";
            SSQL = SSQL + " ) as s";
            SSQL = SSQL + " PIVOT ( sum(Total) FOR [Shift] IN ([GENERAL], [SHIFT1], [SHIFT2], [SHIFT3]),[SHIFT4],[SHIFT5],[SHIFT6] ) AS pivot_tbl order by Dept";

            DataCell = objdata.RptEmployeeMultipleDetails(SSQL);

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Day_Department_Abstract.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //Get Company Name
            SSQL = "Select * from Company_Mst Where CompCode='" + SessionCcode + "'";
            DataTable DT_For = new DataTable();
            DT_For = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_For.Rows.Count != 0)
            {
                report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + DT_For.Rows[0]["CompName"].ToString() + "'";
            }
            report.DataDefinition.FormulaFields["Location_Name"].Text = "'" + SessionLcode.ToString() + "'";
            if (Division.ToUpper().ToString() != "-Select-".ToUpper().ToString())
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + Division.ToString() + "'";
            }
            report.DataDefinition.FormulaFields["Date"].Text = "'" + Date.ToString() + "'";
            //Division
            //    Date

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
